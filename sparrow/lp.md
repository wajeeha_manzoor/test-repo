title: sparrow
authors:
  - wajeehamanzoor
categories:
  - docker
description: Just an Introduction plus some Basics
state: draft
isDraft: true
isAvailable: true
isNew: false
isPremium: true
license: Apache
type: public
scenarioCount: 0
scenarios: []
thumbnail: https://brainarator.s3.amazonaws.com/new/pngs/docker.png
