categories:
  - Golang
authors:
  - wajeehamanzoor
description: Lets learn how to develop a concurrent application in Go language.
isDraft: false
isAvailable: true
isNew: false
isPremium: false
license: Apache
scenarioCount: 2
state: draft
scenarios:
  - male
  - female
thumbnail: https://brainarator.s3.amazonaws.com/new/pngs/go.png
title: cat
