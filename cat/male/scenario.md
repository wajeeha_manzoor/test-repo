title: male cat
description: Learn the basics of Goroutines.
time: 15 minutes
level: intermediate
thumbnail: https://brainarator.s3.amazonaws.com/go.svg
steps:
- step1.md
- step2.md
- step3.md
- step4.md
- step5.md
- step6.md
license: ""
isPremium: true
isNew: false
isAvailable: false
authors: []
isDraft: false
state: draft
environmentvars: {}
environment: ""
image: ""
